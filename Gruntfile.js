'use strict';
var opt = require('./options');
module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      dist: [
        'www-deploy/',
      ]
    },
    copy: {
      dist: {
        files: [
          {expand: true, cwd: 'www/', src: ['**'], dest: 'www-deploy/'}
        ]
      }
    },
    requirejs: {
      compile: {
        options: opt
      }
    },
    recess: {
      dist: {
        options: {
          compile: true,
          compress: true
        },
        files: {
          'www/assets/css/main.css': [
            'www/assets/less/main.less'
          ]
        }
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'www/assets/js/*.js',
        'www/assets/js/mylibs/*.js',
        'www/assets/js/mylibs/*/**.js'
      ]
    },
    watch: {
      less: {
        files: [
          'www/assets/less/*.less',
          'www/assets/less/*/**.less'
        ],
        tasks: ['recess']
      },
      js: {
        files: [
          '<%= jshint.all %>'
        ],
        tasks: ['jshint']
      }
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-recess');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'copy',
    'requirejs'
  ]);

  grunt.registerTask('dev', [
    'watch'
  ]);

};