// set the scene size
var WIDTH = 400,
    HEIGHT = 300;

// set some camera attributes
var VIEW_ANGLE = 45,
    ASPECT = WIDTH / HEIGHT,
    NEAR = 0.1,
    FAR = 10000;

// get the DOM element to attach to
// - assume we've got jQuery to hand
var $container = document.getElementById("container");

    // create a WebGL renderer, camera
    // and a scene
    var renderer = new THREE.WebGLRenderer();
    var camera = new THREE.PerspectiveCamera(  VIEW_ANGLE,
                                    ASPECT,
                                    NEAR,
                                    FAR  );
    var scene = new THREE.Scene();

    // the camera starts at 0,0,0 so pull it back
    camera.position.z = 1800;

    // start the renderer
    renderer.setSize(WIDTH, HEIGHT);

    // attach the render-supplied DOM element
    $container.appendChild(renderer.domElement);

    var geometry =  new THREE.DodecahedronGeometry( 200, 1 );
    var material = new THREE.MeshLambertMaterial(
    {
        color: 0xCC0000
    });
    var cube = new THREE.Mesh( geometry, material );
    scene.add( cube );

    // and the camera
    scene.add(camera);

    // create a point light
    var pointLight = new THREE.PointLight( 0xFFFFFF );

    // set its position
    pointLight.position.x = 10;
    pointLight.position.y = 50;
    pointLight.position.z = 4;

    // add to the scene
    scene.add(pointLight);

function render() {
    cube.rotation.x += 0.05;
    cube.rotation.y += 0.05;

    requestAnimationFrame(render);
    renderer.render(scene, camera);
}
render();