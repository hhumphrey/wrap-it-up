require([
    'app/router',
    'ember'
],

function( Router ) {
    var App = Ember.Application.create({
        Router: Router
    });
});
