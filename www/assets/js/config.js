require.config({
    deps: ["main"],
    paths: {
        jquery: 'lib/require/require-jquery',
        ember: 'lib/ember',
        handlebars: 'lib/handlebars',
        text: 'lib/require/text',
    },
    shim: {
        ember: {
            deps: ["jquery", "handlebars"],
            exports: "Ember"
        }
    }
});
