# Wrap it up

Part javascript practive, part KL10 project the main aim of which is to create a unique wrapping paper based on user input.

Phase 1:

* Simple user interface allowing for the initial user input
* Unique pattern produced from this input
* Wrapping paper is created by repeating this pattern
* Allow for the user to print or save this pattern

Phase 2:

* Allow a choice of patterns
* Allow for further tweeking once the pattern has been produced

Phase 3

* Allow for image upload to create a trully unique wrapping paper